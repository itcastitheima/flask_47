# from toutiao import home_bp
from toutiao.apps.home import home_bp

@home_bp.route('/home')
def home():
    return 'home'


from flask_restful import Resource

class HomeResouce(Resource):

    def get(self):

        return {'msg':'home get'}


"""
需求分析   -----------    MySQL的查询    ---------------  Redis缓存实现


需求:  获取所有的频道.  获取可见的 根据sequence排序

前端:
        发送一个ajax请求获取数据
后端:
        GET app/v1_0/channels
        1.查询数据
        2.将对象列表转换为字典列表
        3.返回响应

"""

from common.models.news import Channel
class AllChannelResouce(Resource):

    def get(self):
        from common.cache.channel import AllChannelCache
        channels_list=AllChannelCache.get_data()
        # 3.返回响应
        return {'channels':channels_list}

"""
需求分析   -----------    MySQL的查询    ---------------  Redis缓存实现


需求:  获取我的频道.  获取我的 未删除的 根据sequence排序

前端:
        发送一个ajax请求获取数据      token
后端:
        GET app/v1_0/user/channels
        
        0.获取用户id
        1.查询数据
        2.将对象列表转换为字典列表
        3.返回响应

"""
from toutiao.utils.decorators import login_required
from common.models.news import UserChannel
from flask import g,request
from common.models.news import UserChannel
from toutiao import db
from common.cache.channel import UserChannelCache
class UserChanelResouce(Resource):

    # method_decorators = [login_required]
    method_decorators = {
        'put': [login_required]
    }
    def get(self):

        # 0.获取用户id
        user_id=g.user_id
        if user_id:

            from common.cache.channel import UserChannelCache
            channels_list=UserChannelCache(user_id).get_data()

            # 3.返回响应
            # return {'channels':channels_list}
        else:

            from common.cache.channel import DefaultUserChannelCache
            channels_list=DefaultUserChannelCache.get_data()
        #这个推荐会影响后边的 个人频道修改
        # 因为这个推荐是前端必须要有的
        channels_list.insert(0,{'id':0,'name':'推荐'})

        return {'channels':channels_list}

    def put(self):
        # 1. 必须是登录用户才可以操作
        user_id=g.user_id
        # 2. 接收数据
        channels=request.json.get('channels')
        # [{id: 21, seq: 2}, {id: 2, seq: 3}, {id: 6, seq: 4}, {id: 4, seq: 5}]
        # 3. 验证数据[省略]
        # 4. 先把当前这个用户的所有关注频道删除,再新增前端提交的
        UserChannel.query.filter(
            UserChannel.user_id == user_id
        ).delete()


        """
        # 全部逻辑删除
        UserChannel.query.filter(
            UserChannel.user_id == user_id,
            UserChannel.is_delete == False
        ).update({is_delete:True})
        
        """

        for item in channels:
            """
            result = UserChannel.query.filter(user_id=user_id,
                                    channel_id=item.get('id')).update({is_delete:False,'sequence':item.get('seq')})
                                    
            result = 1 表示有一个记录修改了
            result = 0 表示没有查询到该记录 ,没有查询到该记录 应该新增数据
            """
            #item = {id: 21, seq: 2}
            uc=UserChannel(
                user_id=user_id,
                channel_id=item.get('id'),
                sequence=item.get('seq')
            )

            db.session.add(uc)
        db.session.commit()
        # 5. 删除之前缓存的频道数据
        UserChannelCache(user_id).delete_data()
        # 6. 返回响应
        return {'channels':channels}
"""
需求:     实现登录用户 关注频道的修改

前端:
        当用户 添加/删除频道的时候,会发送ajax请求
        请求中携带是 当前用户关注的所有频道和顺序
        
后端:
        put app/v1_0/user/channels
        
        1. 必须是登录用户才可以操作
        2. 接收数据 
        3. 验证数据[省略]
        4. 先把当前这个用户的所有关注频道删除,再新增前端提交的
        5. 删除之前缓存的频道数据
        6. 返回响应


"""


########################################
"""
需求:
        首页分类数据的获取

前端:
        当用户点击不同的分类的时候, 发送ajax请求. 
        携带分类id. 
后端:
        GET app/v1_0/articles?channel_id=xxx
        1.接收参数
        2.验证参数
        3.根据参数查询数据
        4.将对象转换为字典
        5.返回响应
            "pre_timestamp": 0,
            "results": [
                {
                    "art_id": 140901,
                    "title": "机器学习技术前沿与未来展望",
                    "aut_id": 1,
                    "pubdate": "2018-11-29T17:18:33",
                    "aut_name": "黑马头条号",
                    "comm_count": 0,
                    "is_top":0,
                    "cover":[{"type": 0, "images": []}]
                }
            ]
"""

from flask import abort
from common.models.news import Article
class IndexResouce(Resource):

    def get(self):
        # 1.接收参数
        channel_id=request.args.get('channel_id')
        if int(channel_id) == 0:
            channel_id = 1
        # 2.验证参数
        # channel = Channel.query.get(channel_id)
        # if channel is None:
        #     abort(404)
        # 3.根据参数查询数据
        articles=Article.query.filter(
            Article.channel_id == channel_id,
            Article.status == Article.STATUS.APPROVED
        ).all()
        # 4.将对象转换为字典
        results=[]
        # flask-restful
        # 传统的方式
        from common.cache.storage import CountReadStorage

        for item in articles:
            temp={
                "art_id": item.id,
                "title": item.title,
                "aut_id": item.user.id,
                "pubdate": item.ctime.strftime('%Y-%m-%d %H:%M:%S'),
                "aut_name": item.user.name,
                "comm_count": CountReadStorage.get_count(item.id),
                "is_top": False,
                'cover': item.cover,
            }
            results.append(temp)
        # 5.返回响应
        return {
            "pre_timestamp": 0,
            "results":results
        }
        #     "pre_timestamp": 0,
        #     "results": [
        #         {
        #             "art_id": 140901,
        #             "title": "机器学习技术前沿与未来展望",
        #             "aut_id": 1,
        #             "pubdate": "2018-11-29T17:18:33",
        #             "aut_name": "黑马头条号",
        #             "comm_count": 0,
        #             "is_top":0,
        #             "cover":[{"type": 0, "images": []}]
        #         }
        #     ]


###################################
# 文章详情

"""

需求:
        实现详情页面的数据展示 
        
前端:
        发送一个ajax请求. 这个请求携带文章id

后端:
        GET app/v1_0/articles/<article_id>
        1. 获取文章id
        2. 根据文章id查询文章信息
        3. 对象转字典
        4. 添加文章作者信息
        5. 初始化 是否关注, 是否点赞 是否收藏
        6. 返回响应

"""

from common.cache.article import ArticleDetailCache
class DetailResouce(Resource):

    def get(self,article_id):
        # 1. 获取文章id
        article_dict = ArticleDetailCache(article_id).get_data()

        if article_dict is None:
            abort(404)
        # 5. 初始化 是否关注, 是否点赞 是否收藏
        is_followed=False    #是否关注
        attitude = None      # 不喜欢0 喜欢1 无态度None
        is_collected=False   #是否收藏

        if g.user_id:
            # 只有登录才能操作的
            from common.cache.user import UserFollowingCache
            is_followed=UserFollowingCache(g.user_id).exist(article_dict['aut_id'])

            from common.cache.user import ArticleAttitudeCache
            attitude=ArticleAttitudeCache(g.user_id).attitude(article_id)

            from common.cache.user import UserHistoryCache
            UserHistoryCache(g.user_id).add(article_id)

            #判断是否收藏该文章
            from common.cache.user import UserCollectionCache
            is_collected=UserCollectionCache(g.user_id).exist(article_id)


        article_dict['is_followed']=is_followed
        article_dict['attitude']=attitude
        article_dict['is_collected']=is_collected
        # 6. 返回响应
        return article_dict

#######################关注取消关注
"""
需求:     实现登录用户关注其他作者

前端:
        当用户点击关注按钮的时候,前端发送一个ajax请求. 
        这个请求携带 token 和关注人的id
后端:
        POST    app/v1_0/user/followings
        
        1. 判断必须是登录用户才能发起请求
        2. 接收参数
        3. 验证参数
        4. 数据入库
        5. 返回响应
"""
from common.models.user import User,Relation
from flask import current_app
class FollowingResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        # 1. 判断必须是登录用户才能发起请求
        # 2. 接收参数
        # target 是关注的那个人的id
        target=request.json.get('target')

        # if target == g.user_id:
        #     abort(400)

        # 3. 验证参数
        try:
            target_user=User.query.get(target)
        except Exception as e:
            current_app.logger.error(e)

        if target is None:
            abort(404)

        # 先查询数据库是否存在数据,如果没有则新增
        # 如果有则更新数据
        relation = Relation.query.filter(
            Relation.user_id == g.user_id,
            Relation.target_user_id == target
        ).first()

        # 判断 是否有查询结果
        if relation is None:
            # 4. 数据入库
            relation=Relation(
                user_id=g.user_id,
                target_user_id=target,
                relation=Relation.RELATION.FOLLOW
            )
            db.session.add(relation)
            db.session.commit()
        else:
            relation.relation = Relation.RELATION.FOLLOW

            db.session.commit()

        #添加缓存
        from common.cache.user import UserFollowingCache
        UserFollowingCache(g.user_id).update(target)

        # 5. 返回响应
        return {'target':target}


#取消关注
"""
需求:     实现登录用户取消关注作者

前端:
        当用户点击取消关注按钮的时候,前端发送一个ajax请求. 
        这个请求携带 token 
后端:
        DELETE    app/v1_0/user/followings/target
    
        1. 必须是登录用户
        2. 接收参数 
        3. 验证参数
        4. 更新状态
        5. 返回响应
"""
from common.cache.user import UserFollowingCache
class UnFollowingResouce(Resource):

    method_decorators = [login_required]

    def delete(self,target):
        # 1. 必须是登录用户
        # 2. 接收参数
        # 3. 验证参数
        # 4. 更新状态
        relation=Relation.query.filter(
            Relation.user_id==g.user_id,
            Relation.target_user_id==target
        ).first()

        #判断 是否有查询结果
        if relation:
            # relation.relation=0  没有错
            relation.relation=Relation.RELATION.DELETE

            db.session.commit()

            #更新缓存
            UserFollowingCache(g.user_id).update(target,action=0)

            # 5. 返回响应
            return {'target':target}
        else:
            abort(400)


#################喜欢和取消不喜欢
"""

需求:     实现登录用户喜欢文章

前端:
        当用户点击喜欢按钮的时候,前端发送一个ajax请求. 
        这个请求携带 token 和 文章的id
后端:
        POST    app/v1_0/article/likings
        
        1. 判断必须是登录用户才能发起请求
        2. 接收参数
        3. 验证参数
        4. 先查询数据库是否有存在的数据
        5. 如果没有,则新增
        6. 如果有,则更新
        7. 返回响应
"""
from common.cache.user import ArticleAttitudeCache
from common.models.news import Attitude
class LikingResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        # 1. 判断必须是登录用户才能发起请求
        user_id=g.user_id
        # 2. 接收参数
        target=request.json.get('target')
        # 3. 验证参数 -- 省略
        # 4. 先查询数据库是否有存在的数据
        at=Attitude.query.filter(
            Attitude.user_id == user_id,
            Attitude.article_id == target
        ).first()
        if at is None:
            # 5. 如果没有,则新增
            at=Attitude(
                user_id=user_id,
                article_id=target,
                attitude=Attitude.ATTITUDE.LIKING
            )
            db.session.add(at)
            db.session.commit()
        else:
            # 6. 如果有,则更新
            # at.attitude=1
            at.attitude=Attitude.ATTITUDE.LIKING
            db.session.commit()

        ArticleAttitudeCache(g.user_id).update(target,attitude=1)
        # 7. 返回响应
        return {'target':target}


#取消点赞
"""
需求:     实现登录用户取消点赞

前端:
        当用户点击取消点赞按钮的时候,前端发送一个ajax请求. 
        这个请求携带 token 
后端:
        DELETE    app/v1_0/article/likings/target

        1. 必须是登录用户
        2. 接收参数 
        3. 验证参数
        4. 更新状态
        5. 返回响应
"""
class DisLikingResouce(Resource):
    method_decorators = [login_required]

    def delete(self,target):
        # 1. 必须是登录用户
        user_id=g.user_id
        # 2. 接收参数
        # 3. 验证参数 -- 省略
        # 4. 更新状态
        at=Attitude.query.filter(
            Attitude.user_id == user_id,
            Attitude.article_id == target
        ).first()
        if at:
            # 没有态度 没有态度是 None
            at.attitude=None
            db.session.commit()

            ArticleAttitudeCache(g.user_id).update(target, attitude=None)

            return {'target':target}
        else:
            abort(400)


##################不喜欢和取消不喜欢
"""

需求:     实现登录用户不喜欢文章

前端:
        当用户点击喜欢按钮的时候,前端发送一个ajax请求. 
        这个请求携带 token 和 文章的id
后端:
        POST    app/v1_0/article/dislikes

        1. 判断必须是登录用户才能发起请求
        2. 接收参数
        3. 验证参数
        4. 先查询数据库是否有存在的数据
        5. 如果没有,则新增
        6. 如果有,则更新
        7. 返回响应
"""
from common.models.news import Attitude


class DislikesResouce(Resource):
    method_decorators = [login_required]

    def post(self):
        # 1. 判断必须是登录用户才能发起请求
        user_id = g.user_id
        # 2. 接收参数
        target = request.json.get('target')
        # 3. 验证参数 -- 省略
        # 4. 先查询数据库是否有存在的数据
        at = Attitude.query.filter(
            Attitude.user_id == user_id,
            Attitude.article_id == target
        ).first()
        if at is None:
            # 5. 如果没有,则新增
            at = Attitude(
                user_id=user_id,
                article_id=target,
                attitude=Attitude.ATTITUDE.DISLIKE
            )
            db.session.add(at)
            db.session.commit()
        else:
            # 6. 如果有,则更新
            # at.attitude=1
            at.attitude = Attitude.ATTITUDE.DISLIKE
            db.session.commit()

        ArticleAttitudeCache(g.user_id).update(target, attitude=0)
        # 7. 返回响应
        return {'target': target}


# 取消不喜欢
"""
需求:     实现登录用户取消不喜欢

前端:
        当用户点击取消点赞按钮的时候,前端发送一个ajax请求. 
        这个请求携带 token 
后端:
        DELETE    app/v1_0/article/dislikes/target

        1. 必须是登录用户
        2. 接收参数 
        3. 验证参数
        4. 更新状态
        5. 返回响应
"""


class UnDisLikesResouce(Resource):
    method_decorators = [login_required]

    def delete(self, target):
        # 1. 必须是登录用户
        user_id = g.user_id
        # 2. 接收参数
        # 3. 验证参数 -- 省略
        # 4. 更新状态
        at = Attitude.query.filter(
            Attitude.user_id == user_id,
            Attitude.article_id == target
        ).first()
        if at:
            # 没有态度 没有态度是 None
            at.attitude = None
            db.session.commit()
            ArticleAttitudeCache(g.user_id).update(target, attitude=None)
            return {'target': target}
        else:
            abort(400)


#####################################发布评论
"""
需求:
        登录用户才可以发布评论
        
前端:
        当登录用户输入一些评论信息之后,回车发表
        前端会发送ajax请求. 请求头中携带 token. 评论信息在请求的body中和文章id

后端:
        POST app/v1_0/comments
        1. 必须是登录用户
        2. 接收参数
        3. 验证参数
        4. 数据入库
        5. 返回响应


"""
from common.models.news import Comment
class CommentResouce(Resource):

    method_decorators = {
        'post':[login_required]
    }

    def post(self):
        # 1. 必须是登录用户
        user_id=g.user_id
        # 2. 接收参数
        # 评论文章为文章id ,对评论进行回复则是评论id
        target=request.json.get('target')
        content=request.json.get('content')

        #对文章评论不需要传递此参数。对评论内容进行回复时，必须传递此参数
        art_id = request.json.get('art_id')

        if art_id is None:
            # 对文章评论
            # 3. 验证参数  -- 省略
            # 4. 数据入库
            cm=Comment(
                user_id=user_id,
                article_id=target,
                content=content
            )

            # 阅读量 +1
            from common.cache.storage import CountReadStorage
            # 我们统一使用 redis缓存来保存阅读量,评论量,转发量等
            # 等到 服务器空闲的时候[2:00~4:00]
            # 我们可以通过定时任务,把redis中的数据读取出来,更新MySQL
            CountReadStorage.update(target)

        else:
            # 评论内容进行回复
            cm=Comment(
                user_id=user_id,
                article_id=art_id,
                parent_id=target,
                content=content
            )

        db.session.add(cm)
        db.session.commit()

        # 5. 返回响应
        return {
            'target':target,
            'comm_id':cm.id
        }

    def get(self):
        # 1. 获取查询字符串
        type=request.args.get('type')

        # 文章id或者评论id
        source=request.args.get('source')

        if type == 'a':
            comments=Comment.query.filter(
                Comment.article_id == source,
                Comment.status == Comment.STATUS.APPROVED,
                Comment.parent_id == None
            ).order_by(Comment.ctime.desc()).all()
            # 2. 获取评论列表
        else:
            comments = Comment.query.filter(
                Comment.parent_id == source,
                Comment.status == Comment.STATUS.APPROVED
            ).order_by(Comment.ctime.desc()).all()
            # 2. 获取回复评论列表

        # 3. 将对象列表转换为字典列表
        results=[]

        from common.cache.user import UserCommitLikingCache

        uclc=UserCommitLikingCache(g.user_id)

        commit_id_list=uclc.get_list()

        for comment in comments:
            results.append({
                'com_id': comment.id,
                'aut_id': comment.user.id,
                'aut_name': comment.user.name,
                'aut_photo': comment.user.profile_photo,
                'pubdate': comment.ctime.strftime('%Y-%m-%d %H:%M:%S'),
                'content': comment.content,
                'is_top': comment.is_top,
                # 'is_liking': uclc.exist(comment.id),
                'is_liking': comment.id in commit_id_list,
                'reply_count': 0
            })
        # 4. 返回响应
        return {
            "total_count": len(comments),
            "end_id": 0,
            "last_id": 0,
            "results": results
        }
         # "total_count": 1,
         # "end_id": 0,
         # "last_id": 0,
         # "results": [
        #      {
        #          "com_id": 108,
        #          "aut_id": 1155989075455377414,
        #          "aut_name": "18310820688",
        #          "aut_photo": "",
        #          "pubdate": "2019-08-07T08:53:01",
        #          "content": "你写的真好",
        #         "is_top": 0,
        #         "is_liking": 0
        #      }
        #  ]
# 获取评论列表
"""
需求:
        都可以获取评论/回复评论 数据

前端:
        
        前端会发送ajax请求. 请求查询字符串  type 评论类型，a表示文章评论 c表示回复评论

后端:
        GET app/v1_0/comments
       1. 获取查询字符串
       2. 获取评论列表
       3. 将对象列表转换为字典列表
       4. 返回响应
        "total_count": 1,
        "end_id": 0, 
        "last_id": 0, 
        "results": [
            {
                "com_id": 108,
                "aut_id": 1155989075455377414,
                "aut_name": "18310820688",
                "aut_photo": "",
                "pubdate": "2019-08-07T08:53:01",
                "content": "你写的真好",
               "is_top": 0,
               "is_liking": 0
            }
        ]


"""
# 评论类型，a表示文章评论 c表示回复评论


#########################实现评论点赞
"""

需求:
        实现登录用户对评论的点赞的功能
前端:
        当登录用户点赞评论的时候,前端会发送ajax请求
        请求携带 token      评论id
后端:
        1.必须是登录用户
        2.接收参数
        3.验证参数
        4.数据更新
        5.返回响应
"""
from common.models.news import CommentLiking
class CommentLikingResouce(Resource):

    method_decorators = [login_required]

    def post(self):
        # 1.必须是登录用户
        user_id=g.user_id
        # 2.接收参数
        comm_id=request.json.get('target')
        # 3.验证参数
        # 4.数据更新
        cl = CommentLiking.query.filter(
            CommentLiking.user_id == user_id,
            CommentLiking.comment_id == comm_id
        ).first()

        if cl:
            cl.is_deleted=False
            db.session.commit()
        else:
            cl = CommentLiking(
                user_id=user_id,
                comment_id=comm_id
            )

            db.session.add(cl)
            db.session.commit()
        # 5.返回响应
        return {
            'target':comm_id
        }


#############################阅读历史
"""

需求:
        登录用户的阅读历史记录. 阅读历史记录 我们保存在 redis中 没有定义模型
前端:
        发送ajax请求. 携带token   
后端:
        
        1. 必须是登录用户
        2. 获取分页参数
            page     int否页数，默认是1
            per_page  int否每页数量
        3. 获取redis中数据
        4. 根据文章id查询文章详细信息
        5. 返回响应
            "page": 1,
            "per_page": 10, 
            "total_count": 100,  
            "results": [
                {
                    "art_id": 108,
                    "title":"Python如何学好"
                    "aut_id": 1155989075455377414,
                    "aut_name": "18310820688",
                    "aut_photo": "",
                    "pubdate": "2019-08-07T08:53:01",
            ]

"""
from common.cache.user import UserHistoryCache
from common.cache.article import ArticleDetailCache
class UserHistoryResouce(Resource):

    method_decorators = [login_required]

    def get(self):
        # 1. 必须是登录用户
        user_id=g.user_id
        # 2. 获取分页参数
        #     page     int否页数，默认是1
        #     per_page  int否每页数量
        page = request.args.get('page',1)
        per_page = request.args.get('per_page',10)

        #  0    10-1=9
        #  10   20-1=19
        #  20   29

        #

        # 3. 获取redis中数据
        # 按 score 值递增(从小到大)来排序
        # redis_cli.zrange(''.format(user_id),(page-1)*per_page,page*per_page-1)

        # reverse

        # 按 score 值递减(从大到小)来排列
        ids = UserHistoryCache(user_id).get_ids(page,per_page)
        # 4. 根据文章id查询文章详细信息
        results=[]
        for id in ids:
            aritcle=ArticleDetailCache(id).get_data()
            results.append(aritcle)

        count=UserHistoryCache(user_id).get_total()

        return {
            "page": page,
            "per_page": per_page,
            "total_count": count,
            "results":results
        }
        # 5. 返回响应
        #     "page": 1,
        #     "per_page": 10,
        #     "total_count": 100,
        #     "results": [
        #         {
        #             "art_id": 108,
        #             "title":"Python如何学好"
        #             "aut_id": 1155989075455377414,
        #             "aut_name": "18310820688",
        #             "aut_photo": "",
        #             "pubdate": "2019-08-07T08:53:01",
        #     ]


##############################收藏列表

"""

需求:
        登录用户的收藏列表. 
前端:
        发送ajax请求. 携带token   
后端:
        GET app/v1_0/article/collections
        
        1. 必须是登录用户
        2. 获取分页参数
            page     int否页数，默认是1
            per_page  int否每页数量
        3. 获取收藏的数据  --- 缓存类
        4. 根据文章id查询文章详细信息
        5. 返回响应
            "page": 1,
            "per_page": 10, 
            "total_count": 100,  
            "results": [
                {
                    "art_id": 108,
                    "title":"Python如何学好"
                    "aut_id": 1155989075455377414,
                    "aut_name": "18310820688",
                    "aut_photo": "",
                    "pubdate": "2019-08-07T08:53:01",
            ]

"""
from common.cache.user import UserCollectionCache
class CollectionResouce(Resource):

    method_decorators = [login_required]

    def get(self):
        # 1. 必须是登录用户
        user_id=g.user_id
        # 2. 获取分页参数
        #     page     int否页数，默认是1
        #     per_page  int否每页数量
        page=request.args.get('page',1)
        per_page=request.args.get('per_page',2)

        try:
            page=int(page)
            per_page=int(per_page)
        except Exception:
            page=1
            per_page=2

        # 3. 获取收藏的数据  --- 缓存类
        ids=UserCollectionCache(user_id).get_ids(page,per_page)
        # 4. 根据文章id查询文章详细信息
        results=[]
        for id in ids:
            article=ArticleDetailCache(id).get_data()
            results.append(article)

        count=UserCollectionCache(user_id).get_total()

        return {
            "page": page,
            "per_page": per_page,
            "total_count": count,
            "results":results
        }
        # 5. 返回响应
        #     "page": 1,
        #     "per_page": 10,
        #     "total_count": 100,
        #     "results": [
        #         {
        #             "art_id": 108,
        #             "title":"Python如何学好"
        #             "aut_id": 1155989075455377414,
        #             "aut_name": "18310820688",
        #             "aut_photo": "",
        #             "pubdate": "2019-08-07T08:53:01",
        #     ]


#########################关注

"""

需求:
        登录用户的关注列表. 
前端:
        发送ajax请求. 携带token   
后端:
        GET app/v_1_0/user/followings?page=xxx&per_page=xxx*
        1. 必须是登录用户
        2. 获取分页参数
            page     int否页数，默认是1
            per_page  int否每页数量
        3. 获取关注数据
        4. 根据人物id查询人物详细信息
        5. 返回响应
            "page": 1,
            "per_page": 10, 
            "total_count": 100,  
            "results": [
                {
                    "id": 1,
                    "name": "黑马头条号",
                    "photo": "Fph9hHDwU9UzMTUAqDwd6bFGaWbg",
                    "fans_count": 1,
                    "mutual_follow": false  # 是否为互相关注
            ]

"""
from common.cache.user import UserInfoCache
class UserFollowingResouce(Resource):

    method_decorators = [login_required]

    def get(self):
        # 1. 必须是登录用户
        user_id=g.user_id
        # 2. 获取分页参数
        #     page     int否页数，默认是1
        #     per_page  int否每页数量
        page = request.args.get('page', 1)
        per_page = request.args.get('per_page', 2)

        try:
            page = int(page)
            per_page = int(per_page)
        except Exception:
            page = 1
            per_page = 2
        # 3. 获取关注数据
        user_ids=UserFollowingCache(user_id).get_ids(page,per_page)
        count=UserFollowingCache(user_id).get_total()
        # 4. 根据人物id查询人物详细信息
        results=[]
        for id in user_ids:

            user_dict=UserInfoCache(id).get()

            results.append({
                "id": user_dict.get('id'),
                "name": user_dict.get('name'),
                "photo": user_dict.get('photo'),
                "fans_count": user_dict.get('fans_count'),
                "mutual_follow": False  # 是否为互相关注
            })

        return  {
            "page": page,
            "per_page": per_page,
            "total_count": count,
            "results":results
        }
        # 5. 返回响应
        #     "page": 1,
        #     "per_page": 10,
        #     "total_count": 100,
        #     "results": [
        #         {
        #             "id": 1,
        #             "name": "黑马头条号",
        #             "photo": "Fph9hHDwU9UzMTUAqDwd6bFGaWbg",
        #             "fans_count": 1,
        #             "mutual_follow": false  # 是否为互相关注
        #     ]

