from flask.blueprints import  Blueprint
# from flask_restful import Api
from common.ttapi.api import BaseApi

home_bp=Blueprint(name='home',import_name=__name__,url_prefix='/app/v1_0')

# 创建API实例对象
home_api=BaseApi(home_bp)

# 把视图[函数]导入到这里
from . import views
# from toutiao.apps.home import views


#添加路由
home_api.add_resource(views.HomeResouce,'/index')

#获取所有频道
home_api.add_resource(views.AllChannelResouce,'/channels')

#获取我的频道
home_api.add_resource(views.UserChanelResouce,'/user/channels')

#获取首页频道列表数据
home_api.add_resource(views.IndexResouce,'/articles')

#文章详细信息
home_api.add_resource(views.DetailResouce,'/articles/<int:article_id>')

#关注用户
home_api.add_resource(views.FollowingResouce,'/user/followings')
#取消关注
home_api.add_resource(views.UnFollowingResouce,'/user/followings/<target>')

#喜欢文章
home_api.add_resource(views.LikingResouce,'/article/likings')

#取消喜欢
home_api.add_resource(views.DisLikingResouce,'/article/likings/<int:target>')


#喜欢文章
home_api.add_resource(views.DislikesResouce,'/article/dislikes')

#取消喜欢
home_api.add_resource(views.UnDisLikesResouce,'/article/dislikes/<int:target>')

#发布评论
home_api.add_resource(views.CommentResouce,'/comments')

#评论点赞
home_api.add_resource(views.CommentLikingResouce,'/comment/likings')

#获取历史记录
home_api.add_resource(views.UserHistoryResouce,'/user/histories')

#收藏列表
home_api.add_resource(views.CollectionResouce,'/article/collections')

#关注
home_api.add_resource(views.UserFollowingResouce,'/user/followings')