from flask_restful import Resource
#3.定义视图

class UserResouce(Resource):

    def get(self):
        return {'msg':'user get'}

"""
小心驶得万年船   好记性不如烂笔头 试一试 试一试又不会怀孕

需求:     实现短信验证码的发送

前端:     当用户点击发送验证码的时候,会发送ajax请求.  发送请求要携带手机号
        
后端:     GET     app/v1_0/sms/codes/<mobile>/
    
        1. 生成随机验证码
        2. 把随机验证码保存起来
        3. 发送短信验证码
        4. 返回响应
"""
from random import randint
from toutiao import redis_cli
from .constants import SMS_CODE_EXPIRE

class SmsCodeResouce(Resource):

    def get(self,mobile):
        # 1. 生成随机验证码
        code = '%06d'%randint(0,999999)
        # 2. 把随机验证码保存起来
        # redis_cli.setex(name,time,value)
        redis_cli.setex(mobile,SMS_CODE_EXPIRE,code)
        # 3. 发送短信验证码 -- 省略 --作业   阿里云 腾讯云
        # 4. 返回响应
        return {
            'mobile':mobile
        }


"""
需求:  登录

前端:
        当用户输入完手机号和短信验证码之后,会点击登录
        会发送ajax请求. 携带 手机号和短信验证码[用户输入的]

后端:
        POST        json [body]
        
        1. 接收数据
        2. 提起数据
        3. 验证数据
        4. 查询数据库.查看是否有这个手机号的用户
        5. 如果没有,则注册--把用户信息保存到数据库中
        6. 如果有,则更新数据--记录一下用户最近登录的时间
        7. 状态保持--token
        8. 返回响应

"""
from flask import request,abort,current_app
from flask_restful import reqparse,inputs
from common.models.user import User
from datetime import datetime
from toutiao import db

class LoginResouce(Resource):

    def post(self):
        # request.json.get('mobile')

        # 1. 接收数据
        # 2. 验证数据
        rp = reqparse.RequestParser()

        rp.add_argument('mobile',
                        required=True,
                        location='json',
                        type=inputs.regex('1[3-9]\d{9}'),
                        help='提示信息')
        rp.add_argument('code',
                        required=True,
                        location='json',
                        # type=inputs.regex('\d{6}'),
                        type=inputs.int_range(100000,999999),
                        help='短信验证码不符合规则')
        # 3. 提起数据
        data = rp.parse_args()

        mobile=data.get('mobile')
        code=data.get('code')

        # 和redis比对
        # redis_code = redis_cli.get(mobile)
        # if redis_code is None:
        #     abort(400)
        #
        # if redis_code.decode() != code:
        #     abort(400)

        # 4. 查询数据库.查看是否有这个手机号的用户
        try:
            user=User.query.filter_by(mobile=mobile).first()
            # user=User.query.filter(User.mobile==mobile).first()
        except Exception as e:
            # current_app 是 app的代理
            # current_app 中肯定有我们在app中设置的日志记录器
            current_app.logger.error(e)

        current_app.logger.info(code)

        if user is None:
            # 5. 如果没有,则注册--把用户信息保存到数据库中
            user=User(
                mobile=mobile,
                name=mobile,
                last_login=datetime.now()
            )

            # from common.snowflake.id_worker import IdWorker
            # worker = IdWorker(1, 2, 0)
            # user.id=worker.get_id()

            # from toutiao import worker
            # user.id=worker.get_id()

            user.id = current_app.worker.get_id()


            db.session.add(user)
            db.session.commit()

        else:
            # 6. 如果有,则更新数据--记录一下用户最近登录的时间
            user.last_login=datetime.now()
            db.session.commit()

        # 登录了之后,缓存用户信息
        from common.cache.user import UserInfoCache
        UserInfoCache(user.id).save(user)

        # 7. 状态保持--token
        from toutiao.utils.jwt import generic_user_token
        token,refresh_token=generic_user_token(user.id)
        # 8. 返回响应
        return {
            'token':token,
            'refresh_token':refresh_token
        }

    def put(self):
        # 如果传递的token 在有效期内 . 有用户的id 并且 is_refresh 是True
        if g.user_id and g.is_refresh:
            from toutiao.utils.jwt import generic_user_token
            #重新生成token
            token, refresh_token = generic_user_token(g.user_id)

            return {
                'token':token
            }
        else:
            return {},403



#查询数据  -- mysql
class UserInfoResource(Resource):

    def get(self):
        user = User.query.filter_by(id=1).first()

        user_data = {
            'id': user.id,
            'mobile': user.mobile,
            'name': user.name,
            'photo': user.profile_photo or '',
            'is_media': user.is_media,
            'intro': user.introduction or '',
            'certi': user.certificate or '',
        }

        return user_data

import json
#查询redis缓存
class CacheUserInfoResource(Resource):

    def get(self):
        redis_data=redis_cli.get(1)
        user_data=json.loads(redis_data.decode())
        return user_data
    #用户第一次将数据库中的数据保存到缓存
    def post(self):
        user = User.query.filter_by(id=1).first()

        user_data = {
            'id': user.id,
            'mobile': user.mobile,
            'name': user.name,
            'photo': user.profile_photo or '',
            'is_media': user.is_media,
            'intro': user.introduction or '',
            'certi': user.certificate or '',
        }

        redis_cli.set(1, json.dumps(user_data))

        return {'message':'ok'}


###################个人中心
"""
需求: 获取个人中心的数据

前端:
        登录用户点击了个人中心
        肯定会发送一个ajax请求. ajax请求肯定携带了token
后端:
        GET     app/v1_0/user
        1. 根据用户id 获取用户信息
        2. 首先从redis中获取
        3. 如果redis中不存在,则查询数据库
        4. 把查询数据库的数据缓存起来,以方便下次使用
        
"""
from flask import g
from toutiao.utils.decorators import login_required
class UserCenterResouce(Resource):

    method_decorators = [login_required]

    def get(self):
        # 问题是 没有用户id

        #模拟一个用户信息.先把 用户中心的功能实现了. 再考虑 如何获取token 如何解析token
        user_id=g.user_id
        # 1. 根据用户id 获取用户信息
        # 2. 首先从redis中获取
        from common.cache.user import UserInfoCache

        return UserInfoCache(user_id).get()

