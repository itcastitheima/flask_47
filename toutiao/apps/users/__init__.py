from flask.blueprints import Blueprint
# from flask_restful import Api
from common.ttapi.api import BaseApi
# 1. 创建蓝图实例对象
users_bp=Blueprint(name='users',import_name=__name__)

# 2. 创建Api实例对象
users_api=BaseApi(users_bp)


#4.添加路由
from . import views

users_api.add_resource(views.UserResouce,'/users')

#短信验证码
users_api.add_resource(views.SmsCodeResouce,'/app/v1_0/sms/codes/<phone:mobile>/')

# flask项目中的url 最后都没有 斜杠
# 添加路由  -- 注册登录
users_api.add_resource(views.LoginResouce,'/app/v1_0/authorizations')


# mysql
users_api.add_resource(views.UserInfoResource,'/mysql')
#redis
users_api.add_resource(views.CacheUserInfoResource,'/redis')

# 获取个人中心数据
users_api.add_resource(views.UserCenterResouce,'/app/v1_0/user')
