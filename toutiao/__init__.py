from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from redis import Redis
from flask_cors import CORS
from toutiao.settings import config_dict,setup_log

app = Flask(__name__)
Config=config_dict.get('dev')

#开启日志
setup_log(Config)

#加载配置类
app.config.from_object(Config)
#创建SQLAlchemy实例对象
db=SQLAlchemy(app)
#创建redis客户端
redis_cli=Redis(host=Config.REDIS_HOST, port=Config.REDIS_PORT,db=Config.REDIS_DB)
#设置cors跨域
CORS(app)

from common.snowflake.id_worker import IdWorker
worker = IdWorker(Config.DATACENTER_ID, Config.WORKER_ID, Config.SEQUENCE)

app.worker=worker

#添加转换器
from toutiao.utils.converters import MobileConverter
app.url_map.converters['phone']=MobileConverter

#注册蓝图
from toutiao.apps.home import home_bp
app.register_blueprint(home_bp)

from toutiao.apps.users import users_bp
app.register_blueprint(users_bp)

from flask import g
from common.token.jwt_auth import check_pyjwt_token
from flask import request
@app.before_request
def before_request():

    auth=request.headers.get('Authorization')

    g.user_id=None
    g.is_refresh=None

    # 我们可以获取 Authorization 的值 [值不为None]
    # 并且以 Bearer
    if auth and auth.startswith('Bearer '):

        # 通过截取字符串来获取token
        token=auth[7:]
        # 对token进行解密
        data=check_pyjwt_token(token)

        if data:
            g.user_id = data.get('user_id')
            g.is_refresh=data.get('is_refresh')


