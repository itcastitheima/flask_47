import logging
import os
from logging.handlers import RotatingFileHandler

#设置 BASE_DIR
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

def setup_log(config):
    """配置日志"""

    # 设置日志的记录等级
    logging.basicConfig(level=config.LOG_LEVEL)  # 调试debug级
    # 创建日志记录器，指明日志保存的路径、每个日志文件的最大大小、保存的日志文件个数上限
    file_log_handler = RotatingFileHandler(os.path.join(BASE_DIR,"logs/log"), maxBytes=1024 * 1024 * 100, backupCount=10)
    # 创建日志记录的格式 日志等级 输入日志信息的文件名 行数 日志信息
    formatter = logging.Formatter('%(levelname)s %(filename)s:%(lineno)d %(message)s')
    # 为刚创建的日志记录器设置日志记录格式
    file_log_handler.setFormatter(formatter)
    # 为全局的日志工具对象（flask app使用的）添加日志记录器
    logging.getLogger().addHandler(file_log_handler)

#定义配置类
class Config(object):
    # KEY 要大写
    DEBUG=True

    # SQLAlchemy的配置
    SQLALCHEMY_DATABASE_URI='mysql://root:mysql@127.0.0.1:3306/toutiao_47'
    SQLALCHEMY_TRACK_MODIFICATIONS=False

    # Redis的配置
    REDIS_HOST='127.0.0.1'
    REDIS_PORT=6379
    REDIS_DB=0

    # 默认日志等级
    LOG_LEVEL = logging.DEBUG

    #秘钥
    SECRET_KEY = '[pojhg3rtyplkzsertyuiol;,mnbvcdrtyuk'
    TOKEN_EXPIRE = 3600
    TOKEN_HOURS = 2
    REFRESH_TOKEN_DAYS = 14

    # Snowflake ID Worker 参数
    DATACENTER_ID = 0
    WORKER_ID = 0
    SEQUENCE = 0

# 线上服务器/生成环境
class ProductionConfig(Config):

    DEBUG = False

    SQLALCHEMY_DATABASE_URI = 'mysql://abc:987@192.168.19.128:3306/toutiao'

    REDIS_HOST = '192.168.19.128'
    REDIS_DB = 10

# 开发环境
class DevelopConfig(Config):

    pass

#测试环境
class TestingConfig(Config):

    pass

config_dict = {
    'prod':ProductionConfig,
    'dev':DevelopConfig,
    'test':TestingConfig
}
