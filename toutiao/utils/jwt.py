from itsdangerous import TimedJSONWebSignatureSerializer as Serializer

from toutiao.settings import Config
from toutiao import Config
from flask import current_app

from common.token.jwt_auth import generic_pyjwt_token
from time import time
from datetime import timedelta,datetime

def generic_user_token(user_id):
    # app.config
    # current_app
    # 1. 创建实例对象
    # s=Serializer(secret_key=current_app.config.get('SECRET_KEY'),expires_in=3600)
    # # 2. 加密数据
    # token=s.dumps({
    #     'id':user_id
    # })
    # # 3. 返回token
    # return token.decode()

    # 2个小时的有效期
    payload = {
        'user_id':user_id,
        'is_refresh':False      #添加一个标记,用于后续区分 token和refresh_token
    }
    expiry= datetime.now() + timedelta(hours=current_app.config.get('TOKEN_HOURS'))
    # expiry 是 datetime
    token=generic_pyjwt_token(payload=payload,expiry=expiry)


    # 14天
    refresh_payload = {
        'user_id': user_id,
        'is_refresh':True       #添加一个标记,用于后续区分 token和refresh_token
    }
    refresh_expiry=datetime.now() + timedelta(days=current_app.config.get('REFRESH_TOKEN_DAYS'))
    refresh_token=generic_pyjwt_token(payload=refresh_payload,expiry=refresh_expiry)


    return token,refresh_token
