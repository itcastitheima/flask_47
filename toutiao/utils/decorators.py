from flask import g

def login_required(func):

    def wrapper(*args,**kwargs):
        if g.user_id is None:
            return {'msg': 'please login'}, 401
        if g.is_refresh:
            return {'msg': 'please login'}, 403

        return func(*args,**kwargs)

    return wrapper
