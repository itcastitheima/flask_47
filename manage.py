# 1. 先进行 Flask-Script的集成
from flask_script import Manager
# 2. 再集成 Flask-Migrate
from flask_migrate import Migrate,MigrateCommand

from toutiao import app,db

manager=Manager(app)

Migrate(app=app,db=db)
manager.add_command('db',MigrateCommand)

if __name__ == '__main__':
    manager.run()
