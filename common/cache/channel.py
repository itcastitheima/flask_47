from common.cache.constants import AllChannelTTL, DefaultUserChannelTTL, UserChannelTTL
from toutiao import redis_cli
from common.models.news import Channel
import json
class AllChannelCache(object):

    # def __init__(self):
    #     self.key='ch:all'

    key='ch:all'

    @classmethod
    def get_data(cls):
        """
        # 1. 先查询redis
        # 2. 判断redis中是否存在缓存数据
        # 3. 如果存在,则使用缓存数据
        # 4. 如果不存在,则查询数据库
        # 5. 把查询的数据放到redis中一份,以备下次使用
        :return:
        """
        # 1. 先查询redis
        data=redis_cli.get(cls.key)
        # 2. 判断redis中是否存在缓存数据
        if data:
            # 将字符串转换为字典列表
            return json.loads(data.decode())
            # 3. 如果存在,则使用缓存数据
        # 4. 如果不存在,则查询数据库
        # 4.1.查询数据
        channels = Channel.query.filter(Channel.is_visible == True).order_by(Channel.sequence).all()
        # 4.2.将对象列表转换为字典列表
        # flask-restful
        # 传统的方式
        channels_list = []
        for channel in channels:
            channels_list.append({
                'id': channel.id,
                'name': channel.name
            })
        # 5. 把查询的数据放到redis中一份,以备下次使用
        # redis_cli.setex(name,time,value)

        # 字典列表转换为字符串
        redis_cli.setex(cls.key,AllChannelTTL.get_value(),json.dumps(channels_list))

        return channels_list


# AllChannelCache.get_data()
# AllChannelCache().get_data()

#默认未登录用户的缓存
class DefaultUserChannelCache(object):

    key = 'ch:user:default'

    @classmethod
    def get_data(cls):
        """
        1. 先查询redis
        2. 判断reids中是否有数据
        3. 如果有,则使用redis
        4. 如果没有,则查询数据库
        5. 查询的数据缓存到redis中一份
        :return:
        """
        # 1. 先查询redis
        data=redis_cli.get(cls.key)
        # 2. 判断reids中是否有数据
        if data:
            return json.loads(data.decode())
            # 3. 如果有,则使用redis
        # 4. 如果没有,则查询数据库
        # 未登录用户查询默认频道
        channels = Channel.query.filter(Channel.is_visible == True,
                                        Channel.is_default == True).order_by(Channel.sequence).all()

        channels_list = []
        for channel in channels:
            channels_list.append({
                'id': channel.id,
                'name': channel.name
            })
        # 5. 查询的数据缓存到redis中一份
        redis_cli.setex(cls.key,DefaultUserChannelTTL.get_value(),json.dumps(channels_list))

        return channels_list


#登录用户的缓存
from common.models.news import UserChannel
class UserChannelCache:

    def __init__(self,user_id):
        self.user_id=user_id
        self.key='user:{}:ch'.format(user_id)


    def get_data(self):

        data=redis_cli.get(self.key)

        if data:
            return json.loads(data.decode())
            # 登录用户查询 用户频道
        # 1.查询数据
        channels = UserChannel.query.filter(UserChannel.is_deleted == False,
                                            UserChannel.user_id == self.user_id).order_by(UserChannel.sequence).all()
        # 2.将对象列表转换为字典列表
        channels_list = []
        for userchannel in channels:
            channels_list.append({
                'id': userchannel.channel.id,
                'name': userchannel.channel.name
            })

        redis_cli.setex(self.key,UserChannelTTL.get_value(),json.dumps(channels_list))


        return channels_list


    def delete_data(self):
        redis_cli.delete(self.key)


