from common.cache.constants import ArticleDetailTTL
from toutiao import redis_cli
import json
from common.models.news import Article
from flask import current_app

from flask_restful import fields,marshal

article_fields = {
    'art_id': fields.Integer(attribute='id'),
    'title': fields.String(attribute='title'),
    'pubdate': fields.DateTime(attribute='ctime', dt_format='iso8601'),
    'content': fields.String(attribute='content.content'),
    'aut_id': fields.Integer(attribute='user_id'),
    'ch_id': fields.Integer(attribute='channel_id'),
}
# 文章详情缓存
class ArticleDetailCache:

    def __init__(self,article_id):
        self.key = 'art:{}:detail'.format(article_id)
        self.article_id=article_id


    def get_data(self):

        data=redis_cli.get(self.key)

        if data:
            return json.loads(data.decode())

        # 2. 根据文章id查询文章信息
        article = None
        try:
            article = Article.query.get(self.article_id)
        except Exception as e:
            current_app.logger.error(e)
        if article is None:
            return None
        # 3. 对象转字典
        article_dict = marshal(article, article_fields)

        # 4. 添加文章作者信息
        article_dict['aut_id'] = article.user.id
        article_dict['aut_name'] = article.user.name
        article_dict['aut_photo'] = article.user.profile_photo

        redis_cli.setex(self.key,ArticleDetailTTL.get_value(),json.dumps(article_dict))


        return article_dict