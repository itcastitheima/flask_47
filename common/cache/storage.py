
"""
    key:
        score: member
        score: member

    阅读量
        90: 777
        87: 666
        50: 555
"""
from toutiao import redis_cli
class CountStorageBase(object):
    key = ''


    # 获取相关的 权重[阅读量,评论量,转发量等]
    @classmethod
    def get_count(cls,article_id):
        # ZSCORE key member
        count = redis_cli.zscore(cls.key,article_id)

        if count:
            return int(count)

    @classmethod
    def update(cls,article_id,increment=1):
        # ZINCRBY key increment member
        redis_cli.zincrby(cls.key,increment,article_id)

class CountReadStorage(CountStorageBase):
    key = 'count:read:storage'

class CountCommentStorage(CountStorageBase):

    key = 'count:comment:storage'