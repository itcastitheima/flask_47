from common.cache.constants import UserCacheTTL
from common.models.user import User
from flask import current_app
import json
from toutiao import redis_cli

class UserInfoCache(object):


    def __init__(self,user_id):
        self.user_id=user_id
        # self.key 就是 保存redis的key
        self.key='user:{}:profile'.format(user_id)


    # 调用save方法的时候,如果有user对象就传递过来
    # 如果没有 我们就根据 user_id查询
    def save(self,user=None):

        if user is None:
            # 根据user_id查询用户信息
            try:
                user=User.query.get(self.user_id)
            except Exception as e:
                current_app.logger.error(e)
                user=None

        if user is not None:
            #将对象转换为字典
            user_info = {
                'id': user.id,
                'mobile': user.mobile,
                'name': user.name,
                'photo': user.profile_photo or '',
                'is_media': user.is_media,
                'intro': user.introduction or '',
                'certi': user.certificate or '',
                'fans_count': user.fans_count or 0,
            }
            #把字典转换为str 保存在redis中
            user_str = json.dumps(user_info)

            # redis_cli.setex(name=,time=,value=)
            redis_cli.setex(name=self.key,time=UserCacheTTL.get_value(),value=user_str)

            return user_info

        else:
            return {}

    def get(self):

        ## 1. 首先从redis中获取
        data=redis_cli.get(self.key)

        if data:
            # 如果有,则使用redis的
            return json.loads(data.decode())
        else:
            # 如果没有,则查询数据库
            return self.save()


#############################关注和取消关注
from common.models.user import Relation
from time import time
class UserFollowingCache(object):

    def __init__(self,user_id):
        self.user_id=user_id
        self.key='user:{}:following'.format(user_id)

    # action=1表示添加关注
    # action=0表示取消关注
    def update(self,target,action=1):
        # ZADD key score member
        # score 是double

        if action == 1:
            # 添加关注
            mapping= {
                # member:score
                target: time()
            }

            redis_cli.zadd(self.key,mapping)

        else:
            # ZREM key member
            redis_cli.zrem(self.key,target)


    def exist(self,target):

        # 1. 获取 有序集合的所有数据
        #ZRANGE key start stop [WITHSCORES]# 返回有序集 key 中，指定区间内的成员
        members=redis_cli.zrange(self.key,0,-1)

        # 2. 判断 target是否存在列表中
        return target in [int(member) for member in members]


    def get_ids(self,page,per_page):
        total_count=self.get_total()

        if total_count>0:
            ids = redis_cli.zrevrange(self.key, (page - 1) * per_page, page * per_page - 1)

            return [int(id) for id in ids]
        else:
            relations=Relation.query.filter(
                Relation.user_id == self.user_id,
                Relation.relation == Relation.RELATION.FOLLOW
            ).order_by(Relation.utime.desc()).all()

            # 为了分页数据
            ids = []
            # 为了缓存
            mapping = {}

            for item in relations:

                ids.append(item.target_user_id)

                mapping[item.target_user_id]=item.utime.timestamp()

            # 5. 把查询的数据缓存起来
            if mapping:
                redis_cli.zadd(self.key, mapping)
                redis_cli.expire(self.key, CollectionTTL.get_value())

            return ids[(page - 1) * per_page:page * per_page]

    def get_total(self):

        return redis_cli.zcard(self.key)


#################对于文章的态度

class ArticleAttitudeCache(object):

    def __init__(self,user_id):

        self.user_id=user_id
        self.key='user:{}:article:attitude'.format(user_id)

    # 文章态度为None的不保存
    # attitude = 1 点赞
    # attitde = 0 不喜欢
    # attitude = None 删除

    def update(self,article,attitude):

        # redis_cli.hset(name,key,value)
        if attitude is None:
            redis_cli.hdel(self.key,article)
        else:
            redis_cli.hset(self.key,article,attitude)


    def attitude(self,article_id):
        """
        根据文章id,获取文章的态度
        :param article_id:  文章id
        :return:
        """
        # attitude = 1 点赞
        #     # attitde = 0 不喜欢
        #     # attitude = None 删除
        data = redis_cli.hget(self.key,article_id)
        if data:
            return int(data)
        else:
            return None


############################用户评论点赞的缓存
from common.models.news import CommentLiking
from .constants import CommentLikingTTL

class UserCommitLikingCache(object):

    def __init__(self,user_id):

        self.user_id=user_id
        self.key='user:{}:comm:liking'.format(user_id)

    #获取到缓存中的所有点赞的评论id
    def get_list(self):

        """
        1. 先查询redis缓存
        2. 判断缓存是否存在
        3. 如果存在,则使用缓存
        4. 如果不存在,则查询数据库
        5. 把查询的数据缓存起来
        :return:
        """
        # 1. 先查询redis缓存
        data=redis_cli.smembers(self.key)
        # 2. 判断缓存是否存在
        if data:
            # data
            return [int(item) for item in data]
            # 3. 如果存在,则使用缓存

        # 4. 如果不存在,则查询数据库
        commentlikings=CommentLiking.query.filter(
            CommentLiking.user_id == self.user_id,
            CommentLiking.is_deleted == False
        ).all()

        comment_id_list=[]
        for item in commentlikings:
            comment_id_list.append(item.comment_id)
        # 5. 把查询的数据缓存起来
        if len(comment_id_list)>0:
            redis_cli.sadd(self.key,*comment_id_list)
            redis_cli.expire(self.key,CommentLikingTTL.get_value())
        # [1,2,3,4,5]
        return comment_id_list

    def exist(self,commit_id):

        commit_id_list = self.get_list()

        return commit_id in commit_id_list


class UserHistoryCache(object):

    def __init__(self,user_id):
        self.user_id=user_id
        self.key='user:{}:history'.format(user_id)

    def get_total(self):

        return redis_cli.zcard(self.key)

    def get_ids(self,page,per_page):

        # 反转获取数据
        # 分页
        ids = redis_cli.zrevrange(self.key,(page-1)*per_page,page*per_page-1)

        return [int(id) for id in ids]


    def add(self,article_id):

        mapping = {
            article_id: time()
        }

        redis_cli.zadd(self.key,mapping)

from common.models.news import Collection
from .constants import  CollectionTTL

class UserCollectionCache(object):
    """
        有序集合

        key:
            score:member
            score:member
            score:member

        1. 先查询redis缓存
        2. 判断缓存是否存在
        3. 如果存在,则使用缓存
        4. 如果不存在,则查询数据库
        5. 把查询的数据缓存起来
        :return:
    """

    def __init__(self,user_id):
        self.user_id=user_id
        self.key='user:{}:collection'.format(user_id)

    def get_total(self):

        return redis_cli.zcard(self.key)

    def get_ids(self,page,per_page):

        #

        # 1. 先查询redis缓存
        total_count=self.get_total()
        # 2. 判断缓存是否存在
        if total_count > 0:

            # 3. 如果存在,则使用缓存
            ids = redis_cli.zrevrange(self.key, (page - 1) * per_page, page * per_page - 1)

            return [int(id) for id in ids]
        else:
            # 4. 如果不存在,则查询数据库
            collections=Collection.query.filter(
                Collection.user_id == self.user_id,
                Collection.is_deleted == False
            ).order_by(Collection.utime.desc()).all()
            # .all()                        1
            # .offset().limit()             2

            # 为了分页数据
            ids=[]
            # 为了缓存
            mapping={}
            for item in collections:
                ids.append(item.article_id)
                # 字典数据[member]=score
                mapping[item.article_id]=item.utime.timestamp()

            # 5. 把查询的数据缓存起来
            if mapping:
                redis_cli.zadd(self.key,mapping)
                redis_cli.expire(self.key,CollectionTTL.get_value())

            return ids[(page-1)*per_page:page*per_page]


    def exist(self,article_id):

        score = redis_cli.zscore(self.key,article_id)

        if score is None:
            return False
        else:
            return True
