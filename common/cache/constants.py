
# USER_CACHE = 300

# 为了后边讲解 缓存雪崩 我们定义一个类

class BaseCacheTTL(object):

    TTL = 300

    @classmethod
    def get_value(cls):
        return cls.TTL


class UserCacheTTL(BaseCacheTTL):

    TTL = 3600

#所有频道的缓存时间
class AllChannelTTL(BaseCacheTTL):

    TTL = 24 * 3600

#默认用户频道的缓存时间
class DefaultUserChannelTTL(BaseCacheTTL):

    TTL = 24 * 3600

#登录用户频道的缓存时间
class UserChannelTTL(BaseCacheTTL):

    TTL = 2 * 3600


#文章详情的缓存时间
class ArticleDetailTTL(BaseCacheTTL):

    TTL =  3600


#文章评论点赞的缓存时间
class CommentLikingTTL(BaseCacheTTL):

    TTL =  3600

#收藏的缓存时间
class CollectionTTL(BaseCacheTTL):

    TTL =  3600