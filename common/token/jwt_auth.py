import jwt

from flask import current_app
"""
>>> import jwt
>>> encoded_jwt = jwt.encode({"some": "payload"}, "secret", algorithm="HS256")
>>> print(encoded_jwt)
eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzb21lIjoicGF5bG9hZCJ9.Joh1R2dYzkRvDkqv3sygm5YyK8Gi4ShZqbhK2gxcs2U


>>> jwt.decode(encoded_jwt, "secret", algorithms=["HS256"])
{'some': 'payload'}

"""

# 加密的方法
def generic_pyjwt_token(payload,expiry,secret=None,algo='HS256'):
    """

    :param payload:     用户要加密的数据      {}
    :param expiry:      token过期时间       设置一个时间戳  秒数 (从现在到1970年1月1号 00:00:00 的秒数)
    :param secret:      秘钥
    :param algo:        算法
    :return:
    """
    # 时间戳
    _payload=payload
    # 不能保存 datetime类型
    _payload.update({
        'exp':expiry
    })

    if secret is None:
        secret=current_app.config.get('SECRET_KEY')

    return jwt.encode(_payload, secret, algorithm=algo)



#解密的方法
def check_pyjwt_token(token,secret=None,algo='HS256'):

    if secret is None:
        secret=current_app.config.get('SECRET_KEY')

    try:
        data = jwt.decode(token,secret , algorithms=[algo])
    except Exception:
        return None

    return data