from flask_restful import Api
from collections import OrderedDict
from .output import custom_output_json

class BaseApi(Api):

    def __init__(self, app=None, prefix='',
                 default_mediatype='application/json', decorators=None,
                 catch_all_404s=False, serve_challenge_on_401=False,
                 url_part_order='bae', errors=None):


        #其他的属性不变.其他的属性还是按照父类的值

        super().__init__(app=app,prefix=prefix,
                         default_mediatype=default_mediatype,decorators=decorators,
                         catch_all_404s=catch_all_404s,serve_challenge_on_401=serve_challenge_on_401,
                         url_part_order=url_part_order,errors=errors)

        # 我们就是为了该其中的一个属性
        self.representations = OrderedDict([('application/json', custom_output_json)])

